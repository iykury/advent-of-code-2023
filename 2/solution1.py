#!/usr/bin/env python3
import sys
import operator
from collections import namedtuple
from functools import reduce

Game = namedtuple("Game", ["id", "bags"])
Bag = namedtuple("Bag", ["red", "green", "blue"])
MAX_BAG = Bag(red = 12, green = 13, blue = 14)

def main():
    with open(sys.argv[1]) as file:
        lines = [line.rstrip("\n") for line in file.readlines()]

    games = map(parse_game, lines)
    possible_games = filter(game_is_possible, games)
    possible_ids = [game.id for game in possible_games]
    total = sum(possible_ids)
    print(total)

# given an input like, for example:
#     "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
# parse_game will return a Game like this:
#     Game(id = 1, bags = [Bag(4, 0, 3), Bag(1, 2, 6), Bag(0, 2, 0)])
def parse_game(s):
    split = s.split(": ")
    game_id = int(split[0][5:])
    bags = parse_bags(split[1])
    return Game(game_id, bags)

def parse_bags(s):
    bag_strings = s.split("; ")
    bags = [parse_bag(bag) for bag in bag_strings]
    return bags

def parse_bag(s):
    cube_strings = s.split(", ")
    return parse_cubes(Bag(0, 0, 0), cube_strings)

def parse_cubes(bag, cubes):
    count_string, color = cubes[0].split(" ")
    count = int(count_string)
    

    if color == "red":
        new_bag = Bag(count, bag.green, bag.blue)
    elif color == "green":
        new_bag = Bag(bag.red, count, bag.blue)
    elif color == "blue":
        new_bag = Bag(bag.red, bag.green, count)
    else:
        print(cubes)

    if cubes[1:]:
        return parse_cubes(new_bag, cubes[1:])
    else:
        return new_bag

def game_is_possible(game):
    bag_possibilities = map(bag_is_possible, game.bags)
    return reduce(operator.and_, bag_possibilities)
    
def bag_is_possible(bag):
    cube_possibilities = [bag[i] <= MAX_BAG[i] for i in range(len(bag))]
    return reduce(operator.and_, cube_possibilities)

if __name__ == "__main__":
    main()
