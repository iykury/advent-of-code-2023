#!/usr/bin/env python
import sys

def main():
	with open(sys.argv[1]) as file:
		lines = file.readlines()
	
	sequences = [to_sequence(l) for l in lines]
	extrapolated_values = [extrapolate(s) for s in sequences]
	total = sum(extrapolated_values)
	print(total)

def to_sequence(line):
	split = line.strip("\n").split(" ")
	return [int(i) for i in split]

def extrapolate(seq):
	if is_zeroes(seq):
		return 0
	else:
		return seq[0] - extrapolate(differences(seq)) # literally only had to change this lol

def differences(seq):
	if len(seq) > 1:
		return [seq[1] - seq[0]] + differences(seq[1:])
	else:
		return []

def is_zeroes(seq):
	for s in seq:
		if s != 0:
			return False
	
	return True

if __name__ == "__main__":
	main()
