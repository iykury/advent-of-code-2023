import System.Environment
import Data.Char

main = do
    fileName : _ <- getArgs
    fileContents <- readFile fileName
    let fileLines = lines fileContents
    putStrLn (show (getTotal fileLines))

getTotal l = sum ints
    where
        onlyDigits = map (filter isDigit) l
        twoDigits = map (\s -> head s : [last s]) onlyDigits
        ints = map read twoDigits
