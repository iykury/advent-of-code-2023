#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
    char *words[] = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    char *line = malloc(64);
    size_t lineLength = 64;
    FILE *fp = fopen(argv[1], "r");
    int first, last;
    int total = 0;

    // loop over lines in file
    while (1) {
        getline(&line, &lineLength, fp);
        if(feof(fp)) break;
        first = last = -1;

        // loop over characters in line
        for (int i = 0; line[i] != '\n'; i++) {
            if (line[i] >= '0' && line[i] <= '9') { // check if character is a digit
                last = line[i] - '0';
            } else {
                for (int j = 1; j < 10; j++) { // check if character is the start of a digit word
                    int length = strlen(words[j]);
                    if (memcmp(line + i, words[j], length) == 0) {
                        last = j;
                        break;
                    }
                }
            }

            if (first == -1) {
                first = last;
            }
        }
        
        total += first * 10 + last;
    }

    printf("%d\n", total);
    fclose(fp);
    free(line);
}

